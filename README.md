# vigila

[![Crates.io](https://img.shields.io/crates/v/vigila.svg?style=flat-square)](https://crates.io/crates/vigila)

[Documentation](https://doc.robigalia.org/vigila)

VGA driver.

## Status

In development.
